import argparse
import time
from os import getenv
import vk
from vk.exceptions import VkAPIError


DEFAULT_LANGUAGE_API_VK = 'ru'
DEFAULT_TIMEOUT_API_VK = 10
DEFAULT_NEWS_RELEVANCE_IN_SEC = 60 * 60 * 24 * 2
DEFAULT_MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED = 10
DELAY_BETWEEN_REQUESTS = 0.34
DELAY_AFTER_TOO_MANY_REQUEST = 5
COUNT_OF_ATTEMPTS_IN_TPY_API_METHOD = 3
PREFIX_SOURCE_IDS = {'users': 'u', 'groups': 'g'}
API_VERSION_VK = '5.92'
APP_ID_VK = getenv('APP_ID_VK')
APP_TOKEN_VK = getenv('APP_TOKEN_VK')
LANGUAGE_API_VK = getenv('LANGUAGE_API_VK', DEFAULT_LANGUAGE_API_VK)
NEWS_RELEVANCE_IN_SEC = int(getenv(
    'NEWS_RELEVANCE_IN_SEC', DEFAULT_NEWS_RELEVANCE_IN_SEC))
MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED = int(getenv(
    'MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED',
    DEFAULT_MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED
))
ID_INPUT_TEXT = 'Pass string with VK user ID (NNNNNN) or link on user page ' \
                 'in VK (https://vk.com/idNNNNNN or https://vk.com/username)'


while True:
    try:
        session = vk.Session(access_token=APP_TOKEN_VK)
        api = vk.API(
            session,
            v=API_VERSION_VK,
            lang=LANGUAGE_API_VK,
            timeout=DEFAULT_TIMEOUT_API_VK,
        )
        break
    except Exception:
        time.sleep(5)


def create_parser():
    parser = argparse.ArgumentParser(
        description='The script is looking for posts that VK user liked.')
    parser.add_argument('-i', '--id', help=f"{ID_INPUT_TEXT}.")
    return parser


def try_api_method(api_method,
                   param,
                   count_attempts=COUNT_OF_ATTEMPTS_IN_TPY_API_METHOD):
    result = {'result': None, 'received_error': None}
    need_to_repeat = True
    attempt = 0
    while need_to_repeat and attempt < count_attempts:
        need_to_repeat = False
        attempt += 1
        try:
            result['result'] = api_method(**param)
            time.sleep(DELAY_BETWEEN_REQUESTS)
        except vk.exceptions.VkAPIError as e:
            result['received_error'] = e
            print(e)
            if e.code == 6:
                need_to_repeat = True
                time.sleep(DELAY_AFTER_TOO_MANY_REQUEST)
        except Exception as e:
            result['received_error'] = e
            print(e)
            need_to_repeat = True
    return result


def get_user_id(link):
    user_id = link
    if 'vk.com/' in link:
        user_id = link.split('/')[-1]
    if not user_id.replace('id', '').isdigit():
        user_id = try_api_method(
            api.utils.resolveScreenName,
            {'screen_name': user_id},
        ).get('result').get('object_id')
    else:
        user_id = user_id.replace('id', '')
    return int(user_id)


def get_can_access_closed(vk_user_id):
    params = {
        'user_ids': vk_user_id,
        'fields': 'online, last_seen, timezone',
    }
    user_info = try_api_method(api.users.get, params).get('result', {})[0]
    return user_info.get('can_access_closed', False)


def get_source_ids(vk_user_id):
    subscriptions = try_api_method(
        api.users.getSubscriptions,
        {'user_id': vk_user_id, 'extended': 0},
    ).get('result')
    groups = try_api_method(
        api.groups.get,
        {'user_id': vk_user_id},
    ).get('result')
    users = try_api_method(
        api.friends.get,
        {'user_id': vk_user_id},
    ).get('result')
    source = [
        subscriptions,
        {'groups': groups},
        {'users': users},
    ]
    formatted_ids = []
    for type_id, prefix_id in PREFIX_SOURCE_IDS.items():
        for item in source:
            if item is None:
                continue
            ids_dict = item.get(type_id) or {}
            ids = ids_dict.get('items', [])
            formatted_ids.extend([f"{prefix_id}{id}" for id in ids])
    formatted_ids = list(set(formatted_ids))
    return formatted_ids


def get_news_feed(source_ids,
                  max_count_of_step=MAX_COUNT_OF_STEPS_OF_GET_NEWS_FEED,
                  news_relevance_in_sec=NEWS_RELEVANCE_IN_SEC):
    posts = []
    news_feed = {'next_from': True}
    step = 0
    while step < max_count_of_step and news_feed.get('next_from'):
        params = {
            'filters': 'post',
            'source_ids': ', '.join(source_ids),
            'count': 100,
            'start_from': news_feed.get('next_from'),
            'start_time': int(time.time()) - news_relevance_in_sec,
        }
        news_feed = try_api_method(api.newsfeed.get, params).get('result')
        posts.extend(news_feed.get('items'))
        step += 1
    if news_feed.get('next_from') is None:
        print(f"News feed ended on the {len(posts)} post. "
              f"Stopped on step number {step}.")
    else:
        print(f'News feed not ended. Stopped by maximum count of step. '
              f'The current number posts in the news feed is {len(posts)}.')
    return posts


def get_liked_posts(vk_user_id, posts):
    liked_posts = []
    step = 0
    for post in posts:
        params = {
            'user_id': vk_user_id,
            'item_id': post.get('post_id'),
            'type': post.get('type'),
            'owner_id': post.get('source_id'),
        }
        result = try_api_method(api.likes.isLiked, params)
        if result.get('received_error'):
            is_liked = 0
        else:
            is_liked = result.get('result', {}).get('liked')
        if is_liked:
            link_to_post = (f"https://vk.com/wall{post.get('source_id')}"
                            f"_{post.get('post_id')}")
            liked_posts.append(link_to_post)
        step += 1
    return liked_posts


def print_links_to_posts(posts):
    if posts:
        print('Liked posts:')
    else:
        print('No liked posts found.')
    for post in posts:
        print(post)


if __name__ == '__main__':
    parser = create_parser()
    namespace = parser.parse_args()
    if namespace.id:
        vk_user_link = namespace.id
    else:
        vk_user_link = input(f"{ID_INPUT_TEXT}:")
    if vk_user_link == '':
        print('User ID is empty!\nExit.')
        exit(0)
    vk_user_id = get_user_id(vk_user_link)
    can_access_closed = get_can_access_closed(vk_user_id)
    if can_access_closed is False:
        print('This profile is private. '
              'Requested information is not available.')
        exit(0)
    source_ids = get_source_ids(vk_user_id)
    posts = get_news_feed(source_ids)
    liked_posts = get_liked_posts(vk_user_id, posts)
    print_links_to_posts(liked_posts)
